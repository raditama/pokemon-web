import { POKEAPI_URL, DEV_URL } from '../common/constans';

function getPokemonList(offset, limit) {
    let url = `${POKEAPI_URL}?offset=${offset}&limit=${limit}`;
    let config = {
        method: 'GET',
    };
    return fetch(url, config)
        .then(response => response.json());
}

function getPokemonDetail(id) {
    const url = `${POKEAPI_URL}${id}`;
    let config = {
        method: 'GET',
    };
    return fetch(url, config)
        .then(response => response.json());
}

function getProbability() {
    const url = `${DEV_URL}probability`;
    let config = {
        method: 'GET',
    };
    return fetch(url, config)
        .then(response => response.json());
}

function getRelease() {
    const url = `${DEV_URL}release`;
    let config = {
        method: 'GET',
    };
    return fetch(url, config)
        .then(response => response.json());
}

function getRename(mount, nickname) {
    const url = `${DEV_URL}rename/${mount}/${nickname}`;
    let config = {
        method: 'GET',
    };
    return fetch(url, config)
        .then(response => response.json());
}

export const Services = {
    getPokemonList, getPokemonDetail, getRename, getProbability, getRelease,
}