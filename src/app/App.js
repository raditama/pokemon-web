import history from '../history';
import { Router as BrowserRouter, Switch, Route } from 'react-router-dom';
import Layout from '../common/layout/layout';

function App() {
  return (
    <BrowserRouter history={history} basename={"/"} >
      <div>
        <Switch>
          <Route component={Layout} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;