import React, { useEffect, useState } from 'react';
import '../../asset/css/styles.css';
import { connect } from 'react-redux';
import history from '../../history';
import { getPokemonList, fetchMyPokemonList } from '../../common/store/general/action';
import { IMAGE_URL } from '../../common/constans';

const MyPokemonPage = (props) => {
    useEffect(() => {
        fetchMyPokemonList();
    }, [])    

    const fetchMyPokemonList = () => {
        props.fetchMyPokemonList();
    }

    return (
        <div className="container-page">
            <div className="container-table">
                <div className="header-table">
                    <p className='text-header-table'>{props.myPokemonList.length} My Pokemons</p>
                </div>
                <div className="body-table">
                    {props.myPokemonList.map((data) => {
                        const imgUrl = `${IMAGE_URL}${data.id}.png`;
                        return (
                            <div className="bar-table" onClick={() => { history.push("/pokemon-detail/" + data.name) }}>
                                <div className='bar-table-1'>
                                    <p>{data.id}</p>
                                </div>
                                <div className='bar-table-1'>
                                    <img height="45" src={imgUrl} />
                                </div>
                                <div className='bar-table-2 margin-left'>
                                    <p>{data.name}</p>
                                </div>
                                <div className='bar-table-2 margin-left'>
                                    <p>{data.nickname}</p>
                                </div>
                            </div>
                        )
                    })}
                </div>
                <div className="footer-table">
                    
                </div >
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    ...state.pokemon,
});

const mapDispatchToProps = (dispatch => ({
    getPokemonList,
    fetchMyPokemonList,
}))();

export default connect(mapStateToProps, mapDispatchToProps)(MyPokemonPage);