import React, { useEffect, useState } from 'react';
import '../../asset/css/styles.css';
import { connect } from 'react-redux';
import history from '../../history';
import { getPokemonList } from '../../common/store/general/action';
import Pagination from '@mui/material/Pagination';
import { IMAGE_URL } from '../../common/constans';

const PokemonListPage = (props) => {
    const [page, setPage] = useState(1);

    const handleChange = (event, value) => {
        setPage(value);
    };

    useEffect(() => {
        const offset = (page - 1) * 10;
        props.getPokemonList(offset, 10);
    }, [page])

    return (
        <div className="container-page">
            <div className="container-table">
                <div className="header-table">
                    <p className='text-header-table'>Show 10 of {props.pokemonList.count} Pokemon</p>
                </div>
                <div className="body-table">
                    {props.pokemonList.results.map((data) => {
                        let split = data.url.split("/");
                        let id = split[split.length - 2];
                        let imgUrl = `${IMAGE_URL}${id}.png`;
                        return (
                            <div className="bar-table" onClick={() => { history.push("/pokemon-detail/" + data.name) }}>
                                <div className='bar-table-1'>
                                    <p>{id}</p>
                                </div>
                                <div className='bar-table-1'>
                                    <img height="45" src={imgUrl} />
                                </div>
                                <div className='bar-table-2 margin-left'>
                                    <p>{data.name}</p>
                                </div>
                            </div>
                        )
                    })}
                </div>
                <div className="footer-table">
                    <div className='footer-table-left'>
                        <p className='text-header-table'>Pagination</p>
                    </div>
                    <div className='footer-table-right'>
                        <Pagination size='small' className='margin-right' count={Math.ceil(props.pokemonList.count / 10)} shape="rounded" page={page} onChange={handleChange} />
                    </div>
                </div >
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    ...state.pokemon,
});

const mapDispatchToProps = (dispatch => ({
    getPokemonList,
}))();

export default connect(mapStateToProps, mapDispatchToProps)(PokemonListPage);