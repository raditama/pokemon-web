import React, { useEffect, useState } from 'react';
import '../../asset/css/styles.css';
import { connect } from 'react-redux';
import { useParams } from 'react-router-dom';
import { setPokemonDetail, getPokemonDetail, setMyPokemonList, saveMyPokemonList, releaseMyPokemon, fetchMyPokemonList, getRename } from '../../common/store/general/action';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import { Services } from '../../services/services';
import Alert from '@mui/material/Alert';

const PokemonDetailPage = (props) => {
    const { id } = useParams();
    const [isGet, setIsGet] = useState(false);
    const [isModalCatch, setIsModalCatch] = useState(false);
    const [isModalRelease, setIsModalRelease] = useState(false);
    const [isModalRename, setIsModalRename] = useState(false);
    const [inputNick, setInputNick] = useState("");
    const [owned, setOwned] = useState(false);
    const [alert, setAlert] = useState('');

    useEffect(() => {
        props.getPokemonDetail(id);
        props.fetchMyPokemonList();

        setIsGet(true);
    }, [])

    useEffect(() => {
        props.myPokemonList.map((data) => {
            if (data.name === id) {
                setOwned(true)
            }
        })
    }, [isGet])

    const catchAction = () => {
        Services.getProbability().then((res) => {
            if (res.response) {
                setIsModalCatch(true);
                props.getPokemonDetail(id);
                props.fetchMyPokemonList();
                setOwned(true)
                setAlert('Catch success!')
            } else {
                setAlert('Catch failed!')
            }
        })
    }

    const releaseAction = () => {
        setIsModalRelease(true)
    }

    const renameAction = () => {
        setIsModalRename(true)
    }

    const confirmRelease = (isConfirm) => {
        if (isConfirm) {
            Services.getRelease().then((res) => {
                if (res.response) {
                    setAlert('Release success!')
                    props.releaseMyPokemon(id);
                    setOwned(false);
                } else {
                    setAlert('Release failed!')
                }
            })
        } else { }
        setIsModalRelease(false);
    }

    const confirmRename = (isConfirm) => {
        if (isConfirm) {
            props.getRename(id);
            setIsModalRename(false);
            setAlert('Rename success!')
        } else {
            setIsModalRename(false);
        }
    }

    const saveAction = () => {
        setIsModalCatch(false);

        var tempList = {
            id: props.pokemonDetail.id,
            name: props.pokemonDetail.name,
            mountChange: 0,
            nickname: inputNick != null && inputNick != undefined && inputNick != "" ? inputNick : props.pokemonDetail.name,
        }

        props.saveMyPokemonList(tempList);
        setOwned(true);
    }

    return (
        <div className="container-page">
            <div className='container-detail-page'>
                {alert ? <Alert
                    severity={ alert.split(' ')[1] === 'success!' ? 'success' : 'error' }
                    onClose={() => { setAlert('') }}
                >{alert}</Alert> : null}

                <div className='container-title-detail'>
                    <h3>Pokemon Detail</h3>
                </div>
                <div className='container-img-detail'>
                    <img className='main-img-detail' src={props?.pokemonDetail?.sprites?.front_default} />
                    <img className='main-img-detail' src={props?.pokemonDetail?.sprites?.back_shiny} />
                    <img className='main-img-detail' src={props?.pokemonDetail?.sprites?.front_default} />
                    <img className='main-img-detail' src={props?.pokemonDetail?.sprites?.front_shiny} />
                </div>
                <div className='container-btn-detail'>
                    <Button className='btn1-detail' color='secondary' variant='contained' disabled={owned} onClick={() => catchAction()}>
                        {!owned ? "Catch" : null}
                    </Button>
                    <Button className='btn2-detail' color='error' variant='contained' disabled={!owned} onClick={() => releaseAction()}>
                        {owned ? "Release" : null}
                    </Button>
                    <Button className='btn1-detail' color='warning' variant='contained' disabled={!owned} onClick={() => renameAction()}>
                        {owned ? "Rename" : null}
                    </Button>
                </div>
                <div className='container-general-info-detail'>
                    <h4>General Info</h4>
                    <p>Name : {id}</p>
                    <p>Base Experience : {props.pokemonDetail.base_experience}</p>
                    <p>Height : {props.pokemonDetail.height}</p>
                    <p>Weight : {props.pokemonDetail.weight}</p>
                </div>
                <div className='container-more-info-detail'>
                    <div className='more-info-detail'>
                        <h3>Moves</h3>
                        <table>
                            <tbody>
                                {
                                    props.pokemonDetailMoves.map((data) => {
                                        return (<tr key={data.move.name} ><td>{data.move.name}</td></tr>)
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                    <div className='more-info-detail'>
                        <h3>Types</h3>
                        <table>
                            <tbody>
                                {
                                    props.pokemonDetailTypes.map((data) => {
                                        return (<tr key={data.type.name} ><td>{data.type.name}</td></tr>)
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <Modal
                open={isModalCatch}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Enter Nickname
                    </Typography>
                    <h1>{inputNick}</h1>
                    <TextField
                        id="filled-helperText"
                        label="Optional"
                        defaultValue={props.pokemonDetail.name}
                        variant="filled"
                        onChange={(e) => setInputNick(e.target.value)}
                    />
                    <div className='margin-top'>
                        <Button variant="contained" onClick={() => saveAction()}>SAVE</Button>
                    </div>

                </Box>
            </Modal>

            <Modal
                open={isModalRelease}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Confirmation
                    </Typography>
                    <h1>{inputNick}</h1>
                    <p>Are you sure you want to release this Pokemon?</p>

                    <Button color="error" onClick={() => confirmRelease(false)}>CANCEL</Button>
                    <Button variant="contained" color="error" onClick={() => confirmRelease(true)}>CONFIRM</Button>
                </Box>
            </Modal>

            <Modal
                open={isModalRename}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Confirmation
                    </Typography>
                    <h1>{inputNick}</h1>
                    <p>Are you sure you want to rename this Pokemon?</p>

                    <Button color="error" onClick={() => confirmRename(false)}>CANCEL</Button>
                    <Button variant="contained" color="error" onClick={() => confirmRename(true)}>CONFIRM</Button>
                </Box>
            </Modal>
        </div>
    )
}

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    p: 4,
};

const mapStateToProps = state => ({
    ...state.pokemon,
});

const mapDispatchToProps = (dispatch => ({
    setPokemonDetail,
    getPokemonDetail,
    setMyPokemonList,
    saveMyPokemonList,
    releaseMyPokemon,
    fetchMyPokemonList,
    getRename,
}))();

export default connect(mapStateToProps, mapDispatchToProps)(PokemonDetailPage);