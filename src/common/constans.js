export const POKEAPI_URL = 'https://pokeapi.co/api/v2/pokemon/';
export const DEV_URL = 'http://localhost:8085/api/pokemon/';
export const IMAGE_URL = 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/';