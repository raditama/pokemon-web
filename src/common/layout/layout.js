import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';

import Header from '../component/header';
import PokemonListPage from '../../app/pokemon-list-page/PokemonListPage';
import MyPokemonPage from '../../app/mypokemon-page/MyPokemonPage';
import PokemonDetailPage from '../../app/pokemon-detail-page/PokemonDetailPage';

class Layout extends Component {
  render() {
    return (
      <div>
        <Header path={this.props.location.pathname} />
        <div>
          <Switch>
            <Route exact path={"/"} component={PokemonListPage} />
            <Route exact path={"/mypokemon"} component={MyPokemonPage} />
            <Route exact path={"/pokemon-detail/:id"} component={PokemonDetailPage} />
          </Switch>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {

  };
}

const mapDispatchToProps = (dispatch) => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);