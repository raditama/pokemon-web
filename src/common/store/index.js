import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { combineReducers } from 'redux';
import pokemon from './general/reducer'

const rootReducer = combineReducers({
    pokemon
});

export const store = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware
    )
);