import * as actionType from './action-type';
import { Services } from '../../../services/services';

const KEY_MYPOKEMON_LIST = "KEY_MYPOKEMON_LIST";

export const setPokemonList = payload => ({
    type: actionType.SET_POKEMON_LIST,
    payload,
});

export const setMyPokemonList = payload => ({
    type: actionType.SET_MYPOKEMON_LIST,
    payload,
});

export const setPokemonDetail = payload => ({
    type: actionType.SET_POKEMON_DETAIL,
    payload,
});

export function getPokemonDetail(id) {
    return dispatch => {
        Services.getPokemonDetail(id)
            .then(res => {
                dispatch(setPokemonDetail(res));
            });
    }
};

export function getPokemonList(offset, limit) {
    return dispatch => {
        Services.getPokemonList(offset, limit)
            .then(res => {
                dispatch(setPokemonList(res));
            });
    }
};

export function fetchMyPokemonList() {
    return dispatch => {
        let data = localStorage.getItem(KEY_MYPOKEMON_LIST);
        let mylist = data === null ? [] : JSON.parse(data);

        dispatch(setMyPokemonList(mylist))
    }
};

export function saveMyPokemonList(data) {
    let local = localStorage.getItem(KEY_MYPOKEMON_LIST);
    let mylist = local === null ? [] : JSON.parse(local);
    let save = mylist.concat(data);

    localStorage.setItem(KEY_MYPOKEMON_LIST, JSON.stringify(save));

    return dispatch => {
        dispatch(setMyPokemonList(save));
    }
};

export function releaseMyPokemon(id) {
    let local = localStorage.getItem(KEY_MYPOKEMON_LIST);
    let mylist = local === null ? [] : JSON.parse(local);
    let remove = -1;


    mylist.map((dt, i) => {
        if (dt.name === id)
            remove = i;
    });

    mylist.splice(remove, 1);

    localStorage.setItem(KEY_MYPOKEMON_LIST, JSON.stringify(mylist));

    return dispatch => {
        dispatch(setMyPokemonList(mylist));
    }
};

export function getRename(param) {
    let data = localStorage.getItem(KEY_MYPOKEMON_LIST);
    let mylist = data === null ? [] : JSON.parse(data);
    let mount = 0;
    let nickname = "";
    let id = 0;

    mylist?.map((data) => {
        if (data.name == param) {
            mount = data.mountChange;
            nickname = data.nickname;
            id = data.id;
        }
    })

    return dispatch => {
        Services.getRename(mount, nickname).then((res) => {
            mylist?.map((data) => {
                if (data.name == param) {
                    data.nickname = res.response;
                    data.mountChange = data.mountChange + 1;
                }
            })
            localStorage.setItem(KEY_MYPOKEMON_LIST, JSON.stringify(mylist));
        })
    }
};

export function getProbability() {
    function a() {
        Services.getProbability().then(response => response.json());
    }

    return a();
};