import * as actionType from './action-type';

const initialState = {
    pokemonList: {
        results: [],
        count: 0
    },
    myPokemonList: [],
    pokemonDetail: [],
    pokemonDetailMoves: [],
    pokemonDetailTypes: [],
}

const handler = (currentState) => {
    const setPokemonList = (pokemonList) => ({ ...currentState, pokemonList });
    const setMyPokemonList = (myPokemonList) => ({ ...currentState, myPokemonList });
    const setPokemonDetail = (pokemonDetail) => ({
        ...currentState,
        pokemonDetail,
        pokemonDetailMoves: pokemonDetail.moves,
        pokemonDetailTypes: pokemonDetail.types,
    });

    return {
        setPokemonList,
        setMyPokemonList,
        setPokemonDetail,
    }
}

export default (state = initialState, action) => {
    const { type, payload } = action;

    switch (type) {
        case actionType.SET_POKEMON_LIST:
            return handler(state).setPokemonList(payload);
        case actionType.SET_MYPOKEMON_LIST:
            return handler(state).setMyPokemonList(payload);
        case actionType.SET_POKEMON_DETAIL:
            return handler(state).setPokemonDetail(payload);
        default:
            return state;
    }
};