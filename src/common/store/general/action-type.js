export const SET_POKEMON_LIST = '@pokemon/set_pokemon_list';
export const SET_MYPOKEMON_LIST = '@pokemon/set_mypokemon_list';
export const SET_POKEMON_DETAIL = '@pokemon/set_pokemon_detail';