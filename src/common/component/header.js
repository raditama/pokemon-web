import React, { Component } from 'react';
import history from '../../history';
import { connect } from 'react-redux';
import '../../asset/css/styles.css';

class Header extends Component {

    render() {
        return (
            <div>
                <div className='container-header'>
                    <div
                        className={this.props.path === "/" || this.props.path === "" ? "menu-header header-active" : "menu-header"}
                        onClick={() => history.push("/")}>
                        <p>Pokemon List</p>
                    </div>
                    <div
                        className={this.props.path === "/mypokemon" || this.props.path === "" ? "menu-header header-active" : "menu-header"}
                        onClick={() => history.push("/mypokemon")}>
                        <p>My Pokemon</p>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ...state.pokemon,
});

const mapDispatchToProps = (dispatch => ({

}))();

export default connect(mapStateToProps, mapDispatchToProps)(Header);